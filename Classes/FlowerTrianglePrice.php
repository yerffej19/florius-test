<?php

namespace Classes;

require_once 'FlowerShapePrice.php';

class FlowerTrianglePrice extends FlowerShapePrice
{

    protected $base;
    protected $height;

    public function __construct($base = null, $height = null)
    {
        if ($base && $base !== 0
            && $height && $height !== 0) {
            $this->setDimension($base, $height);
        }
    }

    public function getArea()
    {
        return ($this->getHeight() * $this->getBase()) / 2;
    }

    public function setDimension($base, $height)
    {
        $this->setBase($base);
        $this->setHeight($height);
    }

    /**
     * @return mixed
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param mixed $base
     */
    public function setBase($base)
    {
        $this->base = $base;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }


}