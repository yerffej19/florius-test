<?php
namespace Classes;

abstract class FlowerShapePrice {
    const DEFAULT_COST_PER_AREA = 2.5;

    protected $costPerArea;
    protected $area;

    abstract public function getArea();

    public function getPrice()
    {
        return $this->getArea() * $this->getCostPerArea();
    }

    public function getCostPerArea()
    {
        return $this->costPerArea ? $this->costPerArea : self::DEFAULT_COST_PER_AREA;
    }

    public function setCostPerArea($costPerArea)
    {
        $this->costPerArea = $costPerArea;
    }



}