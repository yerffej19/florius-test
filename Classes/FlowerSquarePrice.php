<?php
namespace Classes;

require_once 'FlowerShapePrice.php';

class FlowerSquarePrice extends FlowerShapePrice {

    protected $length;

    public function __construct($length = null)
    {
        if ($length && $length !== 0) {
            $this->setLength($length);
        }
    }

    public function getArea()
    {
        return pow($this->getLength(),2);
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }



}