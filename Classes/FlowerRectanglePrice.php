<?php

namespace Classes;

require_once 'FlowerShapePrice.php';

class FlowerRectanglePrice extends FlowerShapePrice
{

    protected $length;
    protected $width;

    public function __construct($length = null, $width = null)
    {
        if ($length && $length !== 0
            && $width && $width !== 0) {
            $this->setDimension($length, $width);
        }
    }

    public function getArea()
    {
        return $this->getWidth() * $this->getLength();
    }

    public function setDimension($length, $width)
    {
        $this->setLength($length);
        $this->setWidth($width);
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }


}