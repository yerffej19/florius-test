<?php

namespace Classes;

require_once 'FlowerShapePrice.php';

class FlowerCirclePrice extends FlowerShapePrice
{
    protected $diameter;

    public function __construct($diameter = null)
    {
        if ($diameter && $diameter !== 0) {
            $this->setDiameter($diameter);
        }
    }

    public function getArea()
    {
        $radius = $this->getDiameter() / 2;

        return pi() * pow($radius,2);
    }

    /**
     * @return mixed
     */
    public function getDiameter()
    {
        return $this->diameter;
    }

    /**
     * @param mixed $diameter
     */
    public function setDiameter($diameter)
    {
        $this->diameter = $diameter;
    }


}