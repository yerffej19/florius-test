<?php
require_once 'Classes/FlowerShapePrice.php';
require_once 'Classes/FlowerSquarePrice.php';
require_once 'Classes/FlowerCirclePrice.php';
require_once 'Classes/FlowerRectanglePrice.php';
require_once 'Classes/FlowerTrianglePrice.php';

use Classes\FlowerCirclePrice;
use Classes\FlowerRectanglePrice;
use Classes\FlowerSquarePrice;
use Classes\FlowerTrianglePrice;

// Square
$square_flower = new FlowerSquarePrice();
$square_flower->setLength(1);

echo $square_flower->getPrice() . "<br>";

// Circle
$circle_flower = new FlowerCirclePrice();
$circle_flower->setDiameter(2);

echo $circle_flower->getPrice() . "<br>";

// Rectangle
$rect_flower = new FlowerRectanglePrice(5, 2);
echo $rect_flower->getPrice() . "<br>";

$rect_flower->setDimension(3, 5);
echo $rect_flower->getPrice() . "<br>";


// Triangle
$triangle_flower = new FlowerTrianglePrice(3, 5);
echo $triangle_flower->getPrice() . "<br>";

// Set cost per area
$square_flower_new_cost = new FlowerSquarePrice(5);
$square_flower_new_cost->setCostPerArea(5);

echo $square_flower_new_cost->getPrice() . "<br>";

